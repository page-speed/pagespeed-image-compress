<?php
/**
 * Plugin Name:       PageSpeed Image Compress
 * Plugin URI:        http://pagespeedimagecompress.com
 * Description:       PageSpeed Image Compress
 * Version:           0.1.0
 * Author:            André, Ivanilton e Jônatan
 * Author URI:        http://pagespeedimagecompress.com
 * Text Domain:       pagespeed-image-compress
 * Requires at least: 4.4
 * Tested up to:      4.7.2
 */
namespace Apiki\PSIC\Manager;

if ( ! defined( 'ABSPATH' ) ) {
	exit(0);
}

class App
{
	const NAME = 'PageSpeed Image Compress';

	const SLUG = 'pagespeed-image-compress';

	const PREFIX = 'psic';

	const NAMESPACE = __NAMESPACE__;

	const FILE = __FILE__;

	const DIR = __DIR__;

	const VERSION = '0.1.0';

	public static function uses( $class, $location )
	{
		$sep  = DIRECTORY_SEPARATOR;
		$root = dirname( __FILE__ );
		$ext  = self::_get_extension( $location );

		self::_require( "{$root}{$sep}{$location}{$sep}{$class}.{$ext}" );
	}

	private static function _get_extension( $location )
	{
		$ext = 'php';

		if ( $location === 'View' ) {
			return strtolower( $location ) . ".{$ext}";
		}

		return $ext;
	}

	private static function _require( $file )
	{
		if ( file_exists( $file ) ) {
			require_once( $file );
		}
	}

	private static function _get_files()
	{
		$directories = implode( ',', array(
			'/Helper/',
			'/Vendor/',
			'/Config/',
			'/Controller/base/',
			'/Model/base/',
			'/Controller/',
		));
		$pattern = sprintf( '%s{%s}*.php', dirname( __FILE__ ), $directories );

		return glob( $pattern, GLOB_BRACE );
	}

	public static function is_index( $file )
	{
		return ( false !== strpos( $file, 'index' ) );
	}

	public static function loader( $file, $activate )
	{
		$instance = new Loader( $file, $activate );
		unset( $instance );
	}

	public static function deactivate()
	{

	}
}
add_action( 'plugins_loaded', array( __NAMESPACE__ . '\App', 'load' ) );

register_activation_hook( __FILE__, array( __NAMESPACE__ . '\App', 'activate' ) );
register_deactivation_hook( __FILE__, array( __NAMESPACE__ . '\App', 'deactivate' ) );