<?php
namespace Apiki\PSIC\Manager;

if ( ! defined( 'ABSPATH' ) ) {
	exit(0);
}

class Core
{
	private static $instance = null;

	public function __construct()
	{
		add_action( 'init', array( __CLASS__, 'load_text_domain' ) );
		add_action( 'restrict_manage_posts' , array( $this, 'manage_posts' ) );
	}

	public static function load_text_domain()
	{
		$basename = Utils::basename();
		load_plugin_textdomain( App::SLUG, false, "{$basename}/languages" );
	}
}